import { styled } from "styled-components";

const Info = styled.span`
    font-size: 12px;
    font-style: italic;
    color: #2f364233;
    cursor: pointer;
    width: fit-content;
`;

export { Info };
