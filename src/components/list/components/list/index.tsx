import styled from "@emotion/styled";

const List = styled.ul`
    width: 100%;
    height: 200px;
    position: relative;
    z-index: 667;
    overflow: scroll;
    background: white;
    scrollbar-gutter: unset;
    border: 0.5px solid #2f3642;
    border-radius: 4px;
    box-shadow: 0 3px 5px rgba(47, 54, 66, 0.2); /* Add box shadow */
`;

export { List };
