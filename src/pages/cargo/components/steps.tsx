import { styled } from "styled-components";

const StepInfo = styled.span`
    color: #2f364233;
    font-size: 11px;
`;

export { StepInfo };
